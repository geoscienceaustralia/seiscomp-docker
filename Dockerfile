# We use a multi-stage docker build because there are a LOT of build-time
# dependencies that aren't required for runtime.

# These global args are used in both stages.
# Explicitly set timezone so that tzdata setup doesn't prompt for input:
ARG TZ=UTC
# Prefix under which we will install SeisComP:
ARG SEISCOMP_ROOT=/opt/seiscomp
# ON to include SeisComP GUI, OFF to exclude:
ARG SC_GLOBAL_GUI=ON

#######################################################################
# First stage "builder" compiles the seiscomp source.
FROM ubuntu:20.04 as builder

# Where should we get the source code?
ARG REPO_ROOT=https://github.com/SeisComP
ARG CONTRIB_REPOS=https://github.com/GeoscienceAustralia/ga-mla
# By default, build with 6 threads:
ARG MAKE_OPTS="-j 6"

# Install build dependencies
RUN apt-get update && \
        TZ=$TZ DEBIAN_FRONTEND=noninteractive apt-get install -y \
        cmake \
        build-essential \
        gfortran \
        git \
        flex \
        libxml2-dev \
        libboost-all-dev \
        libssl-dev \
        libncurses-dev \
        libmysqlclient-dev \
        lsb-release \
        python3-dev \
        python3-numpy \
        qtbase5-dev \
        libqt5svg5-dev

# Download seiscomp source (and any extra plugins)
RUN git clone $REPO_ROOT/seiscomp.git /git \
    && cd /git/src/base \
    && git clone $REPO_ROOT/seedlink.git \
    && git clone $REPO_ROOT/common.git \
    && git clone $REPO_ROOT/main.git \
    && git clone $REPO_ROOT/extras.git \
    && git clone $REPO_ROOT/contrib-gns.git \
    && git clone $REPO_ROOT/contrib-ipgp.git \
    && git clone $REPO_ROOT/contrib-sed.git \
    && cd /git/src/extras \
    && for repo in ${CONTRIB_REPOS}; do git clone $repo; done

# Configure cmake and compile
RUN mkdir /git/build \
    && cd /git/build \
    && cmake -S .. -B . -DSC_GLOBAL_GUI=$SC_GLOBAL_GUI -DCMAKE_INSTALL_PREFIX=$SEISCOMP_ROOT \
    && make $MAKE_OPTS \
    && make install

#######################################################################
# Second stage "full" produces a ready-to-run image with minimal deps.
FROM ubuntu:20.04 AS full

# Install the built SeisComP from the builder image
COPY --from=builder $SEISCOMP_ROOT $SEISCOMP_ROOT

# Install barebones config file
COPY global.cfg $SEISCOMP_ROOT/etc/global.cfg

# Set up environment in the docker-native way
ENV TZ=$TZ \
    DATACENTER_ID=TEST \
    AGENCY_ID=TEST \
    ORGANIZATION=TEST \
    CORE_PLUGINS=dbmysql \
    DB_TYPE=mysql \
    DB_USER=sysop \
    DB_PASSWORD=sysop \
    DB_HOST=db \
    DB_PORT=3306 \
    DB_NAME=seiscomp \
    SEISCOMP_ROOT=$SEISCOMP_ROOT \
    PATH="$SEISCOMP_ROOT/bin:${PATH}" \
    LD_LIBRARY_PATH="$SEISCOMP_ROOT/lib:${LD_LIBRARY_PATH}" \
    PYTHONPATH="$SEISCOMP_ROOT/lib/python:${PYTHONPATH}" \
    MANPATH="$SEISCOMP_ROOT/share/man:${MANPATH}"

# Install runtime dependencies
RUN apt-get update \
    && sed 's/install/install -y/g' $SEISCOMP_ROOT/share/deps/ubuntu/20.04/install-base.sh | sh \
    && sed 's/install/install -y/g' $SEISCOMP_ROOT/share/deps/ubuntu/20.04/install-fdsnws.sh | sh \
    && ( [ $SC_GLOBAL_GUI = "ON" ] && sed 's/install/install -y/g' $SEISCOMP_ROOT/share/deps/ubuntu/20.04/install-gui.sh | sh || true) \
    && apt-get clean autoclean && rm -r /var/lib/apt/lists/*

# Stop seiscomp from nagging us about running as root
RUN sed -i 's/asRoot = False/asRoot = True/' $SEISCOMP_ROOT/bin/seiscomp-control.py

# Launch into an interactive shell by default
CMD ["bash"]
