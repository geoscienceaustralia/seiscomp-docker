docker build -t seiscomp:cli-latest -t seiscomp:cli --build-arg SC_GLOBAL_GUI=OFF .
docker build -t seiscomp:gui-latest -t seiscomp:gui -t seiscomp:latest --build-arg SC_GLOBAL_GUI=ON .

v=$(docker run -it seiscomp:cli scmaster --version|sed -En 's/^Framework:\s+([0-9\.]+).*/\1/p')
docker tag seiscomp:cli seiscomp:cli-$v
docker tag seiscomp:gui seiscomp:gui-$v
docker tag seiscomp:latest seiscomp:$v
